require 'json'
require 'date'
require 'securerandom'

require 'pry'

module Parse
  class MusicTerm
    attr_accessor :file_path, :filename, :rows, :data

    def self.build
      music_terms = []
      Dir.glob("data/music_terms/*.txt") do |file_path|
        music_terms << new(file_path)
      end

      music_terms.each do |music_term|
        File.open("api/music_terms/#{music_term.filename}.json", "w") do |f|
          f.write(music_term.data.to_json)
        end
      end
    end

    def initialize(file_path)
      @file_path = file_path
      raise "file not found" unless File.exists?(file_path)
      @filename = File.basename(file_path, ".txt")

      rows = []
      File.open(file_path, "r") do |f|
        f.each_line do |line|
          rows << line.strip
        end
      end

      @data = []
      rows.each_slice(2) do |a, b|
        @data.push([a, b])
      end
    end
  end
end
