require 'json'

module Parse
  class Lesson
    attr_accessor :data, :filename

    def self.build
      lessons = []
      Dir.glob("data/lessons/*.txt") do |file_path|
        lessons << new(file_path)
      end

      lessons.each do |lesson|
        File.open("api/lessons/#{lesson.filename}.json", "w") do |f|
          f.write(lesson.data.to_json)
        end
      end
    end

    def initialize(file_path)
      raise "file not found" unless File.exists?(file_path)
      @filename = File.basename(file_path, ".txt")

      rows = []
      File.open(file_path, "r") do |f|
        f.each_line do |line|
          line.strip!

          next if line.empty?
          rows << line
        end
      end

      @data = []
      rows.each_slice(3) do |imageUrl, title, body|
        @data << {
          imageUrl: "data/lessons/#{imageUrl}",
          title: title,
          body: body,
        }
      end
    end
  end
end
