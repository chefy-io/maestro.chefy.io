require 'json'
require 'date'
require 'securerandom'

module Parse
  class SightReading
    attr_accessor :file_path, :clef, :data

    def self.build(key)
      readings = []
      Dir.glob("data/sight_reading/base/#{key}/*.png") do |file_path|
        readings << new(:base, file_path)
      end

      Dir.glob("data/sight_reading/treble/#{key}/*.png") do |file_path|
        readings << new(:treble, file_path)
      end

      filename = "api/sight_reading/base/#{key.upcase}.json"
      File.open(filename, "w") do |f|
        json_data = readings.select{|r| r.clef == :base }.map(&:data).to_json
        f.write(json_data)
      end

      filename = "api/sight_reading/treble/#{key.upcase}.json"
      File.open(filename, "w") do |f|
        json_data = readings.select{|r| r.clef == :treble }.map(&:data).to_json
        f.write(json_data)
      end

      filename = "api/sight_reading/both/#{key.upcase}.json"
      File.open(filename, "w") do |f|
        json_data = readings.map(&:data).to_json
        f.write(json_data)
      end
    end

    def initialize(clef, file_path)
      @file_path = file_path
      @clef = clef
      raise "file not found" unless File.exists?(file_path)
      basename = File.basename(file_path, ".png")

      scale = basename.match(/^(a|b|c|d|e|f|g)/).captures[0]
      accent = basename.match(/_(s|f)/) && basename.match(/_(s|f)/).captures[0]
      @data = { file_path: @file_path, clef: @clef, note: note(scale, accent)}
    end

    def note(scale, accent)
      case [scale, accent]
      when ["c", "s"]
        "C#/Db"
      when ["c", "f"]
        "B"
      when ["c", nil]
        "C"
      when ["d", "s"]
        "D#/Eb"
      when ["d", "f"]
        "C#/Db"
      when ["d", nil]
        "D"
      when ["e", "s"]
        "F"
      when ["e", "f"]
        "D#/Eb"
      when ["e", nil]
        "E"
      when ["f", "f"]
        "E"
      when ["f", "s"]
        "F#/Gb"
      when ["f", nil]
        "F"
      when ["g", "s"]
        "G#/Ab"
      when ["g", "f"]
        "F#/Gb"
      when ["g", nil]
        "G"
      when ["a", "s"]
        "A#/Bb"
      when ["a", "f"]
        "G#/Ab"
      when ["a", nil]
        "A"
      when ["b", "f"]
        "A#/Bb"
      when ["b", "s"]
        "C"
      when ["b", nil]
        "B"
      end
    end
  end
end
