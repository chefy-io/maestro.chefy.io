require 'json'
require 'date'
require 'securerandom'

require 'pry'

module Parse
  class EarTraining
    attr_accessor :file_path, :filename, :training_type, :data

    def self.build
      %w(intervals chords triads).each do |training_type|
        ear_trainings = []

        Dir.glob("data/ear_training/#{training_type}/*.wav") do |file_path|
          ear_trainings << new(file_path)
        end

        File.open("api/ear_training/#{training_type}.json", "w") do |f|
          f.write(ear_trainings.map(&:data).to_json)
        end
      end
    end

    def initialize(file_path)
      raise "file not found" unless File.exists?(file_path)
      @filename = File.basename(file_path, ".wav")
      @training_type = fetch_training_type(file_path)
      @data = [
        "data/ear_training/#{@training_type}/#{@filename}.wav",
        display_name,
      ]
    end

    def display_name
       case @training_type
       when "intervals"
         @filename.split("_").first(2).join(" ")
       when "chords"
         @filename.split("-")[0..-2].join(" ")
       when "triads"
         @filename.split("-").first + " triad"
       end
    end

    def fetch_training_type(file_path)
      %w(chords intervals triads).each do |training_type|
        return training_type if file_path.include?(training_type)
      end
    end
  end
end
