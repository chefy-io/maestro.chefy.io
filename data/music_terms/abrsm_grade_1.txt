Accelerando
Gradually Getting Quicker
Adagio
Slow or Leisurely
Allegretto
Fairly Quick
Allegro
Quick
Andante
Medium Walking Speed
Cantabile
Singing Style
Capo
Beginning
Crescendo
Gradually Getting Louder
Da,Del
From the
Da Capo
From the Beginning
Dal Segno
Repeat from the sign
Decrescendo
Gradually Getting Quieter
Diminuendo
Gradually Getting Quieter
Fine
The End
Forte
Loud
Fortissimo
Very Loud
Legato
Smoothly
Lento
Slow
Mezzo
Half
Mezzo Forte
Moderately Loud
Mezzo Piano
Moderately Quiet
Moderato
Moderately
Piano
Quiet
Pianissimo
Very Quiet
Poco
A little
Rallentando
Gradually Getting Slower
Ritenuto
Held Back
Segno
Sign
Staccato
Detached
Tempo
Speed
