require_relative '../../lib/parse/sight_reading'
require 'pry'

describe Parse::SightReading do
  let(:sight_reading_path_one) { "data/sight_reading/base/c/a_1.png" }
  let(:sight_reading_path_two) { "data/sight_reading/base/c/b_f_3.png" }
  let(:sight_reading_path_three) { "data/sight_reading/base/c/c_s_3.png" }

  describe "perform" do
    it "returns JSON data" do
      sight_reading_one = described_class.new(:base, sight_reading_path_one)
      sight_reading_two = described_class.new(:base, sight_reading_path_two)
      sight_reading_three = described_class.new(:base, sight_reading_path_three)

      expect(sight_reading_one.data[:note]).to eq("A")
      expect(sight_reading_two.data[:note]).to eq("A#/Bb")
      expect(sight_reading_three.data[:note]).to eq("C#/Db")
    end
  end
end
