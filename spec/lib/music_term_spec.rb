require_relative '../../lib/parse/music_term'
require 'pry'

describe Parse::MusicTerm do
  let(:music_term_path) { "data/music_terms/abrsm_grade_1.txt" }

  describe "perform" do
    it "returns JSON data" do
      music_term = described_class.new(music_term_path)

      expect(music_term.filename).to match(/abrsm_grade_1/)
      expect(music_term.data).to be_a(Array)
      expect(music_term.data[0]).to be_a(Array)
    end
  end
end
