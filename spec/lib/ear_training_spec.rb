require_relative '../../lib/parse/ear_training'
require 'pry'

describe Parse::EarTraining do
  describe "perform" do
    context "intervals" do
      let(:ear_training_path) { "data/ear_training/intervals/perfect_octave_1.wav" }
      it "returns JSON data" do
        ear_training = described_class.new(ear_training_path)

        expect(ear_training.data).to eq(["data/ear_training/intervals/perfect_octave_1.wav", "perfect octave"])
      end
    end

    context "chords" do
      let(:ear_training_path_one) { "data/ear_training/chords/half-diminished-7-1.wav" }
      let(:ear_training_path_two) { "data/ear_training/chords/major-7-1.wav" }
      it "returns JSON data" do
        ear_training_one = described_class.new(ear_training_path_one)
        ear_training_two = described_class.new(ear_training_path_two)

        expect(ear_training_one.data).to eq(["data/ear_training/chords/half-diminished-7-1.wav", "half diminished 7"])
        expect(ear_training_two.data).to eq(["data/ear_training/chords/major-7-1.wav", "major 7"])
      end
    end

    context "triads" do
      let(:ear_training_path) { "data/ear_training/triads/major-6.wav" }
      it "returns JSON data" do
        ear_training = described_class.new(ear_training_path)

        expect(ear_training.data).to eq(["data/ear_training/triads/major-6.wav", "major triad"])
      end
    end
  end
end
