require_relative '../../lib/parse/lesson'
require 'pry'

describe Parse::Lesson do
  let(:lesson_path) { "data/lessons/fundamentals_of_music.txt" }

  describe "perform" do
    it "returns JSON data" do
      lesson = described_class.new(lesson_path)
      expect(lesson.data.length).to eq(6)

      lesson.data.each do |item|
        expect(item[:imageUrl]).to include("data/lessons/images")
        expect(item[:title]).to be_a(String)
        expect(item[:body]).to be_a(String)
      end
    end
  end
end
