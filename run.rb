#!/usr/bin/env ruby

require_relative "lib/parse"

puts "building API for sight reading..."
Parse::SightReading.build("c")
Parse::SightReading.build("g")
Parse::SightReading.build("d")
Parse::SightReading.build("a")
Parse::SightReading.build("e")
Parse::SightReading.build("b")
Parse::SightReading.build("f")
Parse::SightReading.build("bb")
Parse::SightReading.build("eb")
Parse::SightReading.build("ab")
Parse::SightReading.build("db")
Parse::SightReading.build("gb")

puts "building API for music term..."
Parse::MusicTerm.build

puts "building API for ear training..."
Parse::EarTraining.build

puts "building API for lessons..."
Parse::Lesson.build
